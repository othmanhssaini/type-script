class Person {

    private firstName: string ;
    private lastName: string ;
    private age: number ;

    constructor(firstName: string, lastName: string , age: number ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

     descriptionPerson( ) {
        console.log(" My first name is " + this.firstName + " , my lastName is  " + this.lastName + " and my age is " + this.age);
    }
}

const p1 = new Person('HSSAINI'  , 'Othmane' , 24) ;
p1.descriptionPerson();