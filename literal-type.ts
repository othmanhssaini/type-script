/*
    literal-type : 
    -->  Les types littéraux de chaîne fonctionnent bien avec les types d'union et les alias de type dans la pratique.
    -->  Le type de littéral de chaîne vous permet de spécifier un ensemble de valeurs de chaîne possibles pour une variable, 
    seules ces valeurs de chaîne peuvent être affectées à une variable. 
    --> TypeScript génère une erreur de compilation si l'on essaie d'attribuer une valeur à la variable qui n'est pas définie par le type littéral de chaîne. 
*/

// Example 1 : union

 //userType = 'user'  ==>  error
 // userType =  'SUPER' ==> error

 //Exemple 2 : union
 // 

 let userType: 'USER' | 'ADMIN' ;

// userType = 'USER' 
userType = 'ADMIN'

 function saveUser(userId : number, type : 'USER' | 'ADMIN') : void {
    if(type == 'USER') {
        console.log("Saving new user " +userId+" as "+type);
    } else {
        console.log("Saving new admin " +userId+" as "+type);

    }
   
 }
 saveUser(10 ,'USER')
 saveUser(50 , 'ADMIN')

 //Exemple 2 :  ALIAS
type BINARY = 0 | 1 ;
let userId : BINARY ;
userId = 0 ;
userId = 1 ;
//userId = 2 ; ==> error