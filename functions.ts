// Exemple 1 : 
 function add(a : number, b : number) {
    return a + b;
 }
 console.log(add(4,9));

 //Exemple 2 :
 let multiply = (a : number, b : number) => a * b; 
 console.log(multiply(2,6));

 // Exemple  : 
 let soustract = (a : number, b : number) : number => {
    return a - b;
 }
 console.log(soustract(2,6));

 //Exemple 4 :
 function formatName(firstName : string, lastName : string) : String {
    return firstName +" "+lastName;
 } 
 console.log(formatName('Hssaini' , 'Othmane'))

 function formatName2(firstName : string, lastName = 'hssaini')  {
    return firstName +" "+lastName; 
 }
 console.log(formatName2('Monsef' , 'Jeffal'))

 //Exemple 4:

 function printName(firstName :String, ...allTheRest :string[]) {
    return firstName +' ' +allTheRest.join(' ')
 }
 console.log(printName('HSSAINI' ,'JEFFAL', 'AMMOR', 'BENGHANEM')) 

 // Exemple 5: function overloading :

 function addValues(val1 : number, val2 : number) : number ;

 function addValues(val1 : String,val2 : String) : String ;

 function addValues(a: any,b: any) {
    return a + b 
 }

 console.log(addValues(5,6));
 console.log(addValues('Hssaini ' ,'Othamne')) ;
  